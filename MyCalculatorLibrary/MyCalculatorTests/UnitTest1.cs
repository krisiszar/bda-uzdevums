using NUnit.Framework;
using MyCalculatorLibrary;

namespace MyCalculatorTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestAdd()
        {
            var a = 5.0;
            var b = 6.0;
            var expected = 11.0;
            var result = Calculator.Add(a, b);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void TestSubstract()
        {
            var a = 5.0;
            var b = 6.0;
            var expected = -1.0;
            var result = Calculator.Substract(a, b);
            Assert.AreEqual(expected, result);
        }
    }
}